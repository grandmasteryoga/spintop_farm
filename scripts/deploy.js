const { ethers: eth } = require("ethers");
const hre = require("hardhat");

/** @type {eth} */
const ethers = hre.ethers;

async function main() {
    // We get the contract to deploy
    const SpintopFarm = await ethers.getContractFactory("FarmSPIN");
    const spintopfarm = await SpintopFarm.deploy();
  
    //const MasterChef = await ethers.getContractFactory("MasterChef");
    //const masterchef = await MasterChef.deploy();

    await spintopfarm.deployed();
    //await masterchef.deployed();

    console.log("Spintop Farm Contract deployed to:", spintopfarm.address);
    //console.log("MasterChef deployed to:", masterchef.address);
  }
  
  main()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });